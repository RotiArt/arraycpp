﻿#include <iostream>

class ClassOne
{
	//модификатор доступа
private:
	//обьявляем переменную a
	int a = 11;
	//public доступный для вызова из вне
public:
	//модуль класса
	ClassOne()
	{
		std::cout << a << "\n";
	}
};

class Vector
{
private:
	double x = 1;
	double y = 2;
	double z = 3;
public:
	void Show()
	{
		std::cout << x << ' ' << y << ' ' << z;
	}
	int ret()
	{
		return sqrt((x+y+z)* (x + y + z));
	}

};

int main()
{
	ClassOne temp;
	Vector v;
	v.Show();
	v.ret();
}